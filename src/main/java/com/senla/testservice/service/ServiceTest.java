package com.senla.testservice.service;

import com.senla.testservice.controller.dto.RequestDto;
import com.senla.testservice.service.dto.GetMethodAnswerDto;
import com.senla.testservice.service.dto.PostPutMethodAnswerDto;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;

@Service
public class ServiceTest {

    public GetMethodAnswerDto get(Integer body, Long timeout) {
        sleep(timeout);

        GetMethodAnswerDto answer = new GetMethodAnswerDto();
        if (Objects.nonNull(body)) {
            if (body == 1) {
                answer.setUuid(UUID.randomUUID().toString());
            } else if (body == 2) {
                answer.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
            } else if (body == 3) {
                answer.setUuid(UUID.randomUUID().toString());
                answer.setTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
            }
        }
        return answer;
    }

    public PostPutMethodAnswerDto post(RequestDto dto, Long timeout) {
        sleep(timeout);
        return getPutPostDto(dto, true);
    }

    public PostPutMethodAnswerDto put(RequestDto dto, Long timeout) {
        sleep(timeout);
        return getPutPostDto(dto, false);
    }

    private PostPutMethodAnswerDto getPutPostDto(RequestDto dto, boolean isPost) {
        LocalDateTime date = dto.getDate();
        PostPutMethodAnswerDto postMethodAnswerDto = new PostPutMethodAnswerDto();
        if (Objects.isNull(dto.getUuid())) {
            if (isPost) {
                postMethodAnswerDto.setMinute(date.getMinute());
            } else {
                postMethodAnswerDto.setMonth(date.getMonthValue());
            }
            return postMethodAnswerDto;
        }

        int countSymbols = dto.getUuid().length();
        if (countSymbols == 36) {
            if (isPost) {
                postMethodAnswerDto.setHour(date.getHour());
            } else {
                postMethodAnswerDto.setYear(date.getYear());
            }
        } else if (countSymbols == 8) {
            if (isPost) {
                postMethodAnswerDto.setSecond(date.getSecond());
            } else {
                postMethodAnswerDto.setDay(date.getDayOfMonth());
            }
        }
        return postMethodAnswerDto;
    }

    @SneakyThrows
    private void sleep(Long timeout) {
        if (timeout != null) {
            Thread.sleep(timeout);
        }
    }
}
