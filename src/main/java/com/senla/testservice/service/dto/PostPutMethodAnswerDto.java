package com.senla.testservice.service.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostPutMethodAnswerDto {
    private Integer hour;
    private Integer minute;
    private Integer second;
    private Integer year;
    private Integer month;
    private Integer day;
}
