package com.senla.testservice.controller;

import com.senla.testservice.controller.dto.ErrorMessageDto;
import com.senla.testservice.controller.dto.ValidationErrorResponse;
import com.senla.testservice.controller.exception.InternalApplicationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;


@RestControllerAdvice
class ValidateParametersController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
        return new ResponseEntity<>("validation error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalApplicationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorMessageDto> handleInternalApplicationException(InternalApplicationException e) {
        return new ResponseEntity<>(new ErrorMessageDto(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return handleExceptionInternal(ex, getValidationErrorResponse(ex.getBindingResult()), headers, HttpStatus.BAD_REQUEST, request);
    }

    private ValidationErrorResponse getValidationErrorResponse(BindingResult bindingResult) {
        List<ValidationErrorResponse.ErrorEntry> errors = bindingResult.getFieldErrors().stream()
                .map(err -> new ValidationErrorResponse.ErrorEntry(err.getField(), err.getDefaultMessage()))
                .collect(Collectors.toList());
        return new ValidationErrorResponse(errors, null);
    }
}
