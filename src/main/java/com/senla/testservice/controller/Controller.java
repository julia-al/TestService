package com.senla.testservice.controller;

import com.senla.testservice.controller.dto.RequestDto;
import com.senla.testservice.controller.exception.InternalApplicationException;
import com.senla.testservice.service.ServiceTest;
import com.senla.testservice.service.dto.GetMethodAnswerDto;
import com.senla.testservice.service.dto.PostPutMethodAnswerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Validated
@RestController
@RequestMapping
public class Controller {

    @Autowired
    private ServiceTest serviceTest;

    @GetMapping("/params")
    public ResponseEntity<GetMethodAnswerDto> get(@RequestParam @Min(1) @Max(3) Integer body,
                                                  @RequestParam(required = false) Long timeout,
                                                  @RequestParam(defaultValue = "200") Integer code) {
        checkCode(code);
        GetMethodAnswerDto answerDto = serviceTest.get(body, timeout);
        return ResponseEntity.status(code).body(answerDto);
    }

    @PostMapping("/params")
    public ResponseEntity<PostPutMethodAnswerDto> post(@RequestBody @Valid RequestDto requestDto,
                                                       @RequestParam(required = false) Long timeout,
                                                       @RequestParam(defaultValue = "200") Integer code) {
        checkCode(code);
        PostPutMethodAnswerDto answerDto = serviceTest.post(requestDto, timeout);
        return ResponseEntity.status(code).body(answerDto);
    }

    @PutMapping("/params")
    public ResponseEntity<PostPutMethodAnswerDto> put(@RequestBody @Valid RequestDto requestDto,
                                                      @RequestParam(required = false) Long timeout,
                                                      @RequestParam(defaultValue = "200") Integer code) {

        checkCode(code);
        PostPutMethodAnswerDto answerDto = serviceTest.put(requestDto, timeout);
        return ResponseEntity.status(code).body(answerDto);
    }

    private void checkCode(Integer code) {
        if (code != null && code == 500) {
            throw new InternalApplicationException("Internal server error");
        }
    }

}
