package com.senla.testservice.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ValidationErrorResponse {

    private List<ErrorEntry> errors;
    private String message;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class ErrorEntry {
        private String field;
        private String message;
    }
}
