package com.senla.testservice.controller.exception;

public class InternalApplicationException extends RuntimeException {

    public InternalApplicationException(String message) {
        super(message);
    }
}
